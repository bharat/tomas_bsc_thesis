# Running the model and traces generator

To generate a model and a traces files for a given codewords with `n` traces we can generate model by running the below command as follows:

``` Bash
python3 generator.py <codewords> <n> <chosen filename>
```
For example, to generate a model for the codewords are 19, 62, 22 with n = 100 traces with the name g we do the following :-

``` Bash
python3 generator.py 19 62 22 100 g
```
Information and statistic about the models in the old folder is given in the table below.

|Model name | # states | # traces | length of each code | # code words | code words | length of sub sequence (s) |
|-----------|----------|----------|---------------------|--------------|------------------------|-------------------------|
|2_2_20 | 12 | 20 | 2 | 2 | 19 28 |  1  |
|2_3_50 | 32 | 50 | 2 | 3 | 19 28 45  | 1  |
|2_4_75 | 80 | 75 | 2 | 4 | 19 28 45 62 |  5 |  
|3_2_20 | 20 | 20 | 3 | 2 | 241 593 |  3  |
|3_3_50 | 56 | 50 | 3 | 3 | 241 593 678 | 12  |
|3_4_75 | 144 | 75 | 3 | 4 | 241 593 678 912 |  13  |
|4_2_20 | 28 | 20 | 4 | 2 | 1234 5678 | 2  |
|4_3_50 | 80 | 50 | 4 | 3 | 1234 5678 9438 | 3  |
|4_4_75 | 208 | 75 | 4 | 4 | 1234 5678 9438 2165 | 18  |
|2_20 | 11 | 20 | 2 | 2 | 11 22 | 7 |
|3_20 | 31 | 20 | 2 | 3 | 11 22 33 | 5 |
|4_20 | 79 | 20 | 2 | 4 | 11 22 33 44 | 6 |
|5_50 | 191 | 50 | 2 | 5 | 11 22 33 44 55 | 6 |
|6_100 | 447 | 100 | 2 | 6 | 11 22 33 44 55 66 | 8 |
|7_150 | 1023 | 150 | 2 | 7 | 11 22 33 44 55 66 77 | 14 |



