// pub(crate) mod lee_yannakakis;
pub mod tree;

use crate::definitions::mealy::{InputSymbol, OutputSymbol};

pub trait AdaptiveDistinguishingSequence {
    /// Given the previous output, returns the next input, or an [`AdsStatus`].
    ///
    /// Initially, `prev_output` will be ``None``, but after that, it is
    /// required to provide the previous output, wrapped in a ``Some``.
    /// # Errors
    /// None.
    fn next_input(&mut self, prev_output: Option<OutputSymbol>) -> Result<InputSymbol, AdsStatus>;

    fn get_print_tree(&self) -> Box<[u8]>;

    fn identification_power(&self) -> f32;

    fn reset_to_root(&mut self);
}

/// Ads "Error" types:
/// 1. Done &Implies; we've hit a leaf,
/// 2. Unexpected &Implies; the prev output was unexpected.
#[allow(clippy::module_name_repetitions)]
#[derive(Debug)]
pub enum AdsStatus {
    /// End of inputs of the ADS
    Done,
    /// Previous output was unexpected in the ADS
    Unexpected,
}
