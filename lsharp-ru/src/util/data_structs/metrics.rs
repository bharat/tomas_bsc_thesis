use core::time::Duration;
use derive_alias::derive_alias;
use derive_builder::Builder;
use derive_getters::Getters;
use derive_more::{Add, AddAssign, From};
use serde::{Deserialize, Serialize};
use shrinkwraprs::Shrinkwrap;

derive_alias! {
    derive_basic_wrapper => #[derive(Clone, Default, Copy, Debug, Eq, Hash, PartialEq, PartialOrd, Ord, Deserialize, Serialize)]
}

derive_basic_wrapper! {
#[derive(Add, From, AddAssign)]
#[derive(Shrinkwrap)]
pub struct InputCounter(usize);
}

derive_basic_wrapper! {
#[derive(Add, From, AddAssign)]
#[derive(Shrinkwrap)]
pub struct ResetCounter(usize);
}

derive_basic_wrapper! {
#[derive(Add, From, AddAssign, Builder)]
#[derive(Getters)]
#[builder(default)]
pub struct LearningCounter {
    inputs: InputCounter,
    resets: ResetCounter,
    duration: Duration,
}
}

derive_basic_wrapper! {
#[derive(Add, From, AddAssign, Builder)]
#[derive(Getters)]
#[builder(default)]
pub struct TestingCounter {
    inputs: InputCounter,
    resets: ResetCounter,
    duration: Duration,
}
}

derive_basic_wrapper! {
#[derive(Add, From, AddAssign, Builder)]
#[derive(Getters)]
#[builder(default)]
pub struct CombinedCounter {
    learning: LearningCounter,
    testing: TestingCounter,
}
}

pub trait Timer {
    type TimeDuration;

    fn start_timer(&mut self);
    fn read_and_reset_timer(&mut self) -> Self::TimeDuration;
}
