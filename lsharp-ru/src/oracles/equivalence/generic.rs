use std::cell::RefCell;
use std::collections::{BTreeMap, BTreeSet, VecDeque};
use std::fmt::Debug;
use std::rc::Rc;

use chrono::Utc;
use itertools::Itertools;
use rand::rngs::StdRng;

use rand::{Rng, SeedableRng};
use rand_distr::{Distribution, WeightedAliasIndex};
use rustc_hash::FxHashMap;
use strum_macros::Display;

use crate::definitions::characterisation::{Characterisation, SeparatingSeqs, WMethod, WpMethod};
use crate::definitions::mealy::{InputSymbol, Mealy, OutputSymbol, State};
use crate::definitions::FiniteStateMachine;
use crate::learner::obs_tree::ObservationTree;
use crate::oracles::membership::Oracle as OQOracle;
use crate::util::access_seqs::{access_sequences, InputSelection, SearchStrategy};
use crate::util::data_structs::arena_tree::ArenaTree;
use crate::util::sequences::{
    FixedInfixGenerator, Order, RandomAccessSequences, RandomInfixGenerator,
};
use crate::util::toolbox;

use super::incomplete::hads::HADSMethod;
use super::{incomplete::hsi::HSIMethod, CounterExample};
use super::{EquivalenceOracle, InfixStyle};

/// Generic factory Equivalence oracle.
///
/// This EO supports the W, Wp, HSI, and HADS oracles.
#[allow(clippy::module_name_repetitions)]
pub struct GenericEO<'a, T> {
    oq_oracle: Rc<RefCell<OQOracle<'a, T>>>,
    params: EOParams,
    rng: StdRng,
}

impl<'a, T> GenericEO<'a, T>
where
    T: ObservationTree<InputSymbol, OutputSymbol> + Sync + Send,
{
    pub fn new(oq_oracle: Rc<RefCell<OQOracle<'a, T>>>, params: EOParams) -> Self {
        let seed = params.seed;
        Self {
            oq_oracle,
            params,
            rng: StdRng::seed_from_u64(seed),
        }
    }
}

impl<'a, T> EquivalenceOracle<'a, T> for GenericEO<'a, T>
where
    T: ObservationTree<InputSymbol, OutputSymbol> + Sync + Send,
{
    fn get_counts(&mut self) -> (usize, usize) {
        RefCell::borrow_mut(&self.oq_oracle).get_counts()
    }

    #[must_use]
    fn find_counterexample_with_lookahead(
        &mut self,
        hypothesis: &Mealy,
        lookahead: usize,
    ) -> CounterExample {
        if hypothesis.states().len() == 1 {
            return self.find_counterexample_initial(hypothesis);
        }

        match self.params.infix_style {
            InfixStyle::Finite => self.run_finite_suite(hypothesis, lookahead),

            InfixStyle::Infinite => self.run_infinite_suite(hypothesis, lookahead),
        }
    }

    fn find_counterexample(&mut self, hypothesis: &Mealy) -> CounterExample {
        self.find_counterexample_with_lookahead(hypothesis, self.params.extra_states)
    }
}

impl<'a, T> GenericEO<'a, T>
where
    T: ObservationTree<InputSymbol, OutputSymbol> + Sync + Send,
{
    fn run_infinite_suite(&mut self, fsm: &Mealy, lookahead: usize) -> CounterExample {
        let access_map = access_sequences(
            fsm,
            InputSelection::rand_with_seed(self.rng.gen()),
            SearchStrategy::Buggy,
        );
        let access_map = access_map.into_iter().collect();
        let mut acc_seq_gen = RandomAccessSequences::new(&access_map, self.params.seed);

        let rig = RandomInfixGenerator::new(
            self.params.seed,
            fsm.input_alphabet().len(),
            lookahead,
            self.params.expected_random_length,
        );
        let mut infix_gen = rig.into_iter();

        let char_map: BTreeMap<State, Vec<Vec<_>>> = self
            .params
            .char_style
            .characterisation_map(fsm)
            .into_iter()
            .map(|(k, v)| (k, v.into_iter().collect_vec()))
            .collect();
        let weight_map = sort_according_to_num_apart(fsm, &char_map);
        let start_time = Utc::now();
        loop {
            let acc = acc_seq_gen.next().expect("Safe");
            let infix = infix_gen.next().expect("Safe");
            let state = {
                let a = fsm.trace(acc).0;
                fsm.trace_from(a, &infix).0
            };
            let dist_seq = {
                let idx = weight_map.get(&state).expect("Safe").sample(&mut self.rng);
                char_map.get(&state).expect("Safe").get(idx).expect("Safe")
            };
            let test = toolbox::concat_slices(&[acc, &infix, dist_seq]);
            let ce = self.run_test(fsm, &test);
            if ce.is_some() {
                return ce;
            }
            let curr_time = Utc::now();
            let elapsed_time = curr_time - start_time;
            let elapsed_time = elapsed_time.num_minutes();
            if elapsed_time > 20 {
                return None;
            }
        }
    }

    fn run_finite_suite(&mut self, fsm: &Mealy, lookahead: usize) -> CounterExample {
        let tree = {
            let p1_tree = phase_1_tests(fsm, lookahead, self.params.char_style);
            phase_2_tests(fsm, lookahead, self.params.char_style, p1_tree)
        };
        tree.iter().find_map(|seq| self.run_test(fsm, &seq))
    }

    /// Specialised case for when the hypothesis is a single-state FSM.
    #[must_use]
    fn find_counterexample_initial(&mut self, hyp: &Mealy) -> CounterExample {
        // When the hypothesis has size one, there's no point in testing A.C,
        // as we will have no characterising set.
        // A similar argument works for A.I.C, as C is empty.
        // A will contain only the empty sequence,
        // since we are already in the initial state;
        // Therefore, the only thing left is I<=(n+1)
        let input_size = hyp.input_alphabet().len();
        let input_alphabet = toolbox::inputs_iterator(input_size).collect_vec();
        match self.params.infix_style {
            InfixStyle::Finite => {
                let fig =
                    FixedInfixGenerator::new(input_alphabet, self.params.extra_states, Order::ASC);
                fig.generate().find_map(|seq| self.run_test(hyp, &seq))
            }
            InfixStyle::Infinite => {
                let rig = RandomInfixGenerator::new(
                    self.params.seed,
                    input_size,
                    self.params.extra_states,
                    self.params.expected_random_length,
                );
                rig.into_iter().find_map(|seq| self.run_test(hyp, &seq))
            }
        }
    }

    /// Run a single test sequence and return whether it is a CE or not.
    #[inline]
    #[must_use]
    fn run_test(&mut self, hyp: &Mealy, input_seq: &[InputSymbol]) -> CounterExample {
        let hyp_output = hyp.trace(input_seq).1.to_vec();
        let sut_output = RefCell::borrow_mut(&self.oq_oracle).output_query(input_seq);
        (hyp_output != sut_output).then_some((input_seq.to_vec(), sut_output))
    }
}

/// State cover tests generator.
fn phase_1_tests(fsm: &Mealy, lookahead: usize, style: CharStyle) -> PrefixTree {
    let mut tree = PrefixTree::default();

    let input_alphabet = fsm.input_alphabet();
    let hyp_states = fsm.states();

    let access_map = access_sequences(fsm, InputSelection::Normal, SearchStrategy::Bfs);
    let access_seqs = access_map.into_values().collect_vec();
    let fig = FixedInfixGenerator::new(input_alphabet, lookahead - 1, Order::ASC);
    let iter_0 = itertools::repeat_n(vec![], hyp_states.len());
    let infix_gen = fig.generate().chain(iter_0);
    let char_map = match style {
        CharStyle::W | CharStyle::Wp | CharStyle::SepSeq => WMethod::characterisation_map(fsm),
        CharStyle::Hsi => HSIMethod::characterisation_map(fsm),
        CharStyle::Hads => HADSMethod::characterisation_map(fsm),
    };

    for i in infix_gen {
        for a in &access_seqs {
            let s = {
                let seq = a.iter().copied().chain(i.iter().copied());
                fsm.get_dest(fsm.initial_state(), seq)
            };
            let idents = char_map.get(&s).expect("Safe");
            let mut seq = toolbox::concat_slices(&[a, &i]);

            let prefix_len = seq.len();
            for chars in idents {
                seq.extend(chars);
                if goes_to_sink(fsm, &seq) {
                    continue;
                }
                tree.insert(&seq);
                seq.truncate(prefix_len);
            }
        }
    }
    tree
}

/// Transition cover tests generator.
fn phase_2_tests(
    fsm: &Mealy,
    lookahead: usize,
    style: CharStyle,
    mut tree: PrefixTree,
) -> PrefixTree {
    let input_alphabet = fsm.input_alphabet();
    let hyp_states = fsm.states();

    let access_map = access_sequences(fsm, InputSelection::Normal, SearchStrategy::Bfs);
    let access_seqs = access_map.into_values().collect_vec();
    let fig = FixedInfixGenerator::new(input_alphabet, lookahead, Order::ASC);
    let iter_0 = itertools::repeat_n(vec![], hyp_states.len());
    let infix_gen = fig.generate().chain(iter_0);
    let char_map = match style {
        CharStyle::W => WMethod::characterisation_map(fsm),
        CharStyle::Hsi => HSIMethod::characterisation_map(fsm),
        CharStyle::Hads => HADSMethod::characterisation_map(fsm),
        CharStyle::Wp => WpMethod::characterisation_map(fsm),
        CharStyle::SepSeq => SeparatingSeqs::characterisation_map(fsm),
    };

    for i in infix_gen {
        for a in &access_seqs {
            let s = {
                let seq = a.iter().copied().chain(i.iter().copied());
                fsm.get_dest(fsm.initial_state(), seq)
            };
            let idents = char_map.get(&s).expect("Safe");
            let mut seq = toolbox::concat_slices(&[a, &i]);

            let prefix_len = seq.len();
            for chars in idents {
                seq.extend(chars);
                if goes_to_sink(fsm, &seq) {
                    continue;
                }
                tree.insert(&seq);
                seq.truncate(prefix_len);
            }
        }
    }
    tree
}

fn sort_according_to_num_apart(
    fsm: &Mealy,
    char_map: &BTreeMap<State, Vec<Vec<InputSymbol>>>,
) -> FxHashMap<State, WeightedAliasIndex<usize>> {
    let mut weight_state_map = FxHashMap::default();
    for s in fsm.states() {
        let other_states: Vec<_> = fsm.states().into_iter().filter(|x| *x != s).collect();
        let char_set_s = char_map.get(&s).expect("Safe");
        let mut seq_score_vec = char_set_s
            .iter()
            .map(|seq| {
                // For each sequence, how many states are apart?
                let s_resp = fsm.trace_from(s, seq).1;
                let apart_cnt = other_states
                    .iter()
                    .filter(|x| {
                        let x_resp = fsm.trace_from(**x, seq).1;
                        x_resp != s_resp
                    })
                    .count();
                (seq, apart_cnt)
            })
            .collect_vec();
        seq_score_vec.sort_unstable_by(|(_, a_apart), (_, b_apart)| b_apart.cmp(a_apart));
        // let weights: Vec<_> = seq_score_vec.iter().map(|x| x.1).collect();
        let weights = std::iter::repeat(1).take(seq_score_vec.len()).collect_vec();
        let weights = WeightedAliasIndex::new(weights).expect("Safe");
        weight_state_map.insert(s, weights);
    }
    weight_state_map
}

fn goes_to_sink(fsm: &Mealy, word: &[InputSymbol]) -> bool {
    let sunk = word.iter().try_fold(fsm.initial_state(), |src, i| {
        let (dest, out) = fsm.step_from(src, *i);
        if out == OutputSymbol::new(u16::MAX) {
            None
        } else {
            Some(dest)
        }
    });
    sunk.is_none()
}

#[derive(Debug, Clone)]
pub struct EOParams {
    pub extra_states: usize,
    pub expected_random_length: usize,
    pub seed: u64,
    pub char_style: CharStyle,
    pub infix_style: InfixStyle,
}

impl EOParams {
    #[must_use]
    pub fn new(
        extra_states: usize,
        expected_random_length: usize,
        seed: u64,
        char_style: CharStyle,
        infix_style: InfixStyle,
    ) -> Self {
        Self {
            extra_states,
            expected_random_length,
            seed,
            char_style,
            infix_style,
        }
    }
}

#[derive(Debug, Clone, Copy, Display)]
pub enum CharStyle {
    W,
    Wp,
    Hsi,
    Hads,
    SepSeq,
}

impl CharStyle {
    #[must_use]
    pub fn characterisation_map(&self, fsm: &Mealy) -> BTreeMap<State, BTreeSet<Vec<InputSymbol>>> {
        match self {
            Self::W => WMethod::characterisation_map(fsm),
            Self::Wp => WpMethod::characterisation_map(fsm),
            Self::Hsi => HSIMethod::characterisation_map(fsm),
            Self::Hads => HADSMethod::characterisation_map(fsm),
            Self::SepSeq => SeparatingSeqs::characterisation_map(fsm),
        }
    }
}

#[derive(Debug, Default)]
struct PrefixNode {
    // An InputSymbol and its corresponding child node.
    children: FxHashMap<InputSymbol, usize>,
}

#[derive(Debug)]
struct PrefixTree {
    tree: ArenaTree<PrefixNode, ()>,
}

impl Default for PrefixTree {
    fn default() -> Self {
        let node = PrefixNode::default();
        let mut tree = ArenaTree::default();
        tree.node(node);
        Self { tree }
    }
}

impl FromIterator<Vec<InputSymbol>> for PrefixTree {
    fn from_iter<T: IntoIterator<Item = Vec<InputSymbol>>>(iter: T) -> Self {
        let mut tree = Self::default();
        for x in iter {
            tree.insert(&x);
        }
        tree
    }
}

impl PrefixTree {
    fn insert(&mut self, word: &[InputSymbol]) -> bool {
        let mut curr_node_idx = 0usize;
        let mut inserted = false;
        for i in word {
            if let std::collections::hash_map::Entry::Occupied(nxt) =
                self.tree.arena[curr_node_idx].val.children.entry(*i)
            {
                curr_node_idx = *nxt.get();
            } else {
                let d_node = PrefixNode::default();
                let x = self.tree.node(d_node);
                self.tree.arena[curr_node_idx].val.children.insert(*i, x);
                curr_node_idx = x;
                inserted = true;
            }
        }
        inserted
    }

    fn iter(&self) -> PrefixTreeIterator {
        self.into_iter()
    }
}

impl<'a> IntoIterator for &'a PrefixTree {
    type Item = Vec<InputSymbol>;

    type IntoIter = PrefixTreeIterator<'a>;

    fn into_iter(self) -> Self::IntoIter {
        PrefixTreeIterator {
            word: Vec::new(),
            // We ignore the first input in the iterator.
            to_visit: VecDeque::from([(InputSymbol::new(u16::MAX), 1, 0)]),
            tree: self,
        }
    }
}

#[derive(Debug)]
struct PrefixTreeIterator<'a> {
    word: Vec<InputSymbol>,
    to_visit: VecDeque<(InputSymbol, usize, usize)>,
    tree: &'a PrefixTree,
}

impl<'a> Iterator for PrefixTreeIterator<'a> {
    type Item = Vec<InputSymbol>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.to_visit.is_empty() {
            return None;
        }
        while let Some((i, depth, c_node)) = self.to_visit.pop_front() {
            self.word.resize(depth - 1, InputSymbol::new(u16::MAX));
            self.word.push(i);
            if self.tree.tree.arena[c_node].val.children.is_empty() {
                let ret = Some(self.word[1..].to_vec());
                self.word.pop();
                return ret;
            }
            for (x, nxt) in &self.tree.tree.arena[c_node].val.children {
                self.to_visit.push_front((*x, depth + 1, *nxt));
            }
        }
        unreachable!("Impossible case when iterating over PrefixTree.");
    }
}

#[cfg(test)]
mod tests {
    use std::path::Path;

    use itertools::Itertools;
    use rstest::rstest;
    use rustc_hash::FxHashSet;

    use crate::definitions::mealy::{InputSymbol, Mealy};
    use crate::definitions::FiniteStateMachine;
    use crate::util::parsers::machine::{read_mealy_from_file, read_mealy_from_file_using_maps};
    use crate::util::sequences::FixedInfixGenerator;
    use crate::util::toolbox;

    use super::{phase_1_tests, phase_2_tests, CharStyle, PrefixTree};

    #[test]
    fn can_create() {
        let x = PrefixTree::default();
        assert_eq!(x.tree.size(), 1);
    }

    #[test]
    fn insertion_and_iteration() {
        let i_str = "[[1,2,3],[1,2],[1],[1,2,5],[1,2,5,6],[1,2,5,4],[6],[7]]";
        let input_seqs: Vec<Vec<InputSymbol>> = serde_json::from_str(i_str).expect("Safe");

        let mut p_tree = PrefixTree::default();
        for x in input_seqs {
            p_tree.insert(&x);
        }

        let e_str = "[[1,2,3],[1,2,5,6],[1,2,5,4],[6],[7]]";
        let e_seqs_set: Vec<Vec<InputSymbol>> = serde_json::from_str(e_str).expect("Safe");
        let e_seqs_set: FxHashSet<_> = e_seqs_set.into_iter().collect();

        let seq_set = p_tree.into_iter().collect();
        assert_eq!(e_seqs_set, seq_set);
    }

    /// Maximum length test.
    ///
    /// The maximum length of a test can be |Q| + k + 1 + |Q| where
    /// |Q| is the size of the FSM and k is the number of extra states.
    #[rstest]
    #[case("tests/src_models/trial.dot")]
    #[case("tests/src_models/w_test.dot")]
    #[case("tests/src_models/hypothesis_23.dot")]
    #[case("tests/src_models/BitVise.dot")]
    fn max_len(#[case] path: &str) {
        let fsm = load_basic_fsm(path);
        let k = 2;
        let max_size = 2 * fsm.states().len() + k + 1;
        let tree = make_test_tree(&fsm, k, super::CharStyle::W);
        for seq in &tree {
            assert!(seq.len() <= max_size);
        }
    }

    fn load_basic_fsm(name: &str) -> Mealy {
        let file_name = Path::new(name);
        read_mealy_from_file(file_name.to_str().expect("Safe")).0
    }

    fn make_test_tree(fsm: &Mealy, k: usize, char_style: CharStyle) -> PrefixTree {
        let tree = phase_1_tests(fsm, k, char_style);
        phase_2_tests(fsm, k, char_style, tree)
    }

    /// Example from Joshua's dissertation for Fig 2.5 and the W-method.
    #[test]
    fn example() {
        let a = InputSymbol::new(0);
        let b = InputSymbol::new(1);
        let c = InputSymbol::new(2);
        let state_cover = vec![vec![], vec![a], vec![a, a], vec![b], vec![b, a]];
        let fig = FixedInfixGenerator::new(vec![a, b, c], 0, crate::util::sequences::Order::ASC);
        let q: Vec<Vec<InputSymbol>> = state_cover
            .iter()
            .cloned()
            .cartesian_product(fig.generate())
            .map(|(x, y)| toolbox::concat_slices(&[&x, &y]))
            .collect();
        let p_u_q = {
            let mut ret = FxHashSet::default();
            ret.extend(q.into_iter());
            ret.extend(state_cover.iter().cloned());
            ret
        };
        let char_set = vec![vec![a, a], vec![a, c], vec![c]];
        let ts: FxHashSet<_> = p_u_q
            .into_iter()
            .cartesian_product(char_set.into_iter())
            .map(|(x, y)| toolbox::concat_slices(&[&x, &y]))
            .collect();
        let tree: PrefixTree = ts.into_iter().collect();
        let result: FxHashSet<_> = tree.iter().collect();
        let expected = vec![
            vec![a, a, a, a, a],
            vec![a, a, a, a, c],
            vec![a, a, a, c],
            vec![a, a, b, a, a],
            vec![a, a, b, a, c],
            vec![a, a, b, c],
            vec![a, a, c, a, a],
            vec![a, a, c, a, c],
            vec![a, a, c, c],
            vec![a, b, a, a],
            vec![a, b, a, c],
            vec![a, b, c],
            vec![a, c, a, a],
            vec![a, c, a, c],
            vec![a, c, c],
            vec![b, a, a, a, a],
            vec![b, a, a, a, c],
            vec![b, a, a, c],
            vec![b, a, b, a, a],
            vec![b, a, b, a, c],
            vec![b, a, b, c],
            vec![b, a, c, a, a],
            vec![b, a, c, a, c],
            vec![b, a, c, c],
            vec![b, b, a, a],
            vec![b, b, a, c],
            vec![b, b, c],
            vec![b, c, a, a],
            vec![b, c, a, c],
            vec![b, c, c],
            vec![c, a, a],
            vec![c, a, c],
            vec![c, c],
        ];
        let expected: FxHashSet<_> = expected.into_iter().collect();
        assert_eq!(expected, result);
    }

    #[rstest]
    #[case(
        "tests/src_models/bitvise_hypothesis_w_wp.dot",
        "tests/src_models/BitVise.dot"
    )]
    fn w_wp_mismatch(#[case] path: &str, #[case] original: &str) {
        let (sul, input_map, output_map) = read_mealy_from_file(original);
        let fsm = read_mealy_from_file_using_maps(path, &input_map, &output_map).expect("Safe");
        let k = 1;
        let wp_tree = make_test_tree(&fsm, k, CharStyle::Wp);
        let wp_finds_ce = wp_tree
            .into_iter()
            .any(|word| word_splits(&fsm, &sul, &word));
        let full_w_tree = make_test_tree(&fsm, k, CharStyle::W);
        let full_w_finds_ce = full_w_tree
            .into_iter()
            .any(|word| word_splits(&fsm, &sul, &word));
        assert!(!wp_finds_ce, "Wp method does not find a CE.");
        assert_eq!(
            wp_finds_ce, full_w_finds_ce,
            "W and Wp methods do not have the same coverage!"
        );
    }

    /// Check if the input sequence `word` splits machines `m1` and `m2`.
    fn word_splits(m1: &Mealy, m2: &Mealy, word: &[InputSymbol]) -> bool {
        let o1 = m1.trace(word).1;
        let o2 = m2.trace(word).1;
        o1 != o2
    }
}
