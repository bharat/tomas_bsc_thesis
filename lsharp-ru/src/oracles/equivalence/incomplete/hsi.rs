use std::collections::{BTreeMap, BTreeSet};

use itertools::Itertools;
use rayon::prelude::{IntoParallelIterator, ParallelIterator};
use rayon::slice::ParallelSliceMut;
use rustc_hash::FxHashSet;

use crate::definitions::characterisation::Characterisation;
use crate::definitions::mealy::{InputSymbol, Mealy, State};
use crate::definitions::FiniteStateMachine;

use super::tree::SplittingTree;

pub struct HSIMethod;

impl HSIMethod {
    fn cons_sep_seq(
        fsm: &Mealy,
        s: State,
        block: &[State],
        tree: &SplittingTree,
    ) -> Vec<InputSymbol> {
        let mut ret = vec![];
        let mut block: FxHashSet<_> = block.iter().copied().collect();
        let mut s = s;
        while block.len() > 1 {
            let blk = block.iter().copied().collect_vec();
            let wit = tree.separating_sequence(&blk);
            assert!(
                wit.is_set(),
                "Could not find separating sequence for a block."
            );
            let wit = wit.seq();
            ret.extend_from_slice(wit);
            let (new_s, s_resp) = fsm.trace_from(s, wit);
            block = block
                .drain()
                .filter_map(|t| {
                    let (t_p, t_resp) = fsm.trace_from(t, wit);
                    (t_resp == s_resp).then_some(t_p)
                })
                .collect();
            s = new_s;
        }
        ret
    }

    fn cons_local_identifiers(
        fsm: &Mealy,
        s: State,
        stree: &SplittingTree,
    ) -> (State, BTreeSet<Vec<InputSymbol>>) {
        let mut hsi_local = BTreeSet::default();
        let mut block: Vec<_> = fsm.states().into_iter().collect();
        while block.len() > 1 {
            let w = Self::cons_sep_seq(fsm, s, &block, stree);
            let s_resp = fsm.trace_from(s, &w).1;
            block.retain(|t| fsm.trace_from(*t, &w).1 == s_resp);
            hsi_local.insert(w);
        }
        (s, hsi_local)
    }
}

impl Characterisation for HSIMethod {
    fn characterisation_map(fsm: &Mealy) -> BTreeMap<State, BTreeSet<Vec<InputSymbol>>> {
        let all_states = fsm.states();
        let stree = SplittingTree::new(fsm, &all_states);
        let char_map: BTreeMap<_, _> = all_states
            .into_par_iter()
            .map(|s| Self::cons_local_identifiers(fsm, s, &stree))
            .collect();
        let mut ret = BTreeMap::default();
        for s in fsm.states() {
            let mut other_states: FxHashSet<_> =
                fsm.states().into_iter().filter(|t| *t != s).collect();
            let local_identifiers = char_map.get(&s).expect("Safe");
            let mut local_identifiers: Vec<_> = local_identifiers.iter().cloned().collect();
            local_identifiers.par_sort_by_key(|b| std::cmp::Reverse(b.len()));
            let mut new_local_idents = BTreeSet::default();
            for char_seq in local_identifiers {
                if other_states.is_empty() {
                    break;
                }
                let s_resp = fsm.trace_from(s, &char_seq).1;
                other_states.retain(|t| fsm.trace_from(*t, &char_seq).1 == s_resp);
                new_local_idents.insert(char_seq);
            }
            ret.insert(s, new_local_idents);
        }
        ret
    }
}
