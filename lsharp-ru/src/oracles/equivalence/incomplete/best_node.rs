use derive_getters::Getters;

use crate::definitions::mealy::InputSymbol;

use super::index::Index;

#[derive(Debug, PartialEq, Eq, Getters, Clone)]
pub(super) struct BestR {
    #[getter(skip)]
    score: usize,
    input: Option<InputSymbol>,
    next: Option<Index>,
}

impl Default for BestR {
    /// `Default` for `BestR` makes the score equal to `usize::MAX`.
    /// We do this because in the ST-IADS alg., lower is better.
    fn default() -> Self {
        Self {
            score: usize::MAX,
            input: None,
            next: None,
        }
    }
}

impl BestR {
    pub(super) fn score(&self) -> usize {
        self.score
    }

    pub(super) fn update<Seq: Into<Option<InputSymbol>>, Ind: Into<Option<Index>>>(
        &mut self,
        input: Seq,
        next: Ind,
        score: usize,
    ) {
        self.input = input.into();
        self.score = score;
        self.next = next.into();
    }

    pub(super) fn construct<Seq: Into<Option<InputSymbol>>, Ind: Into<Option<Index>>>(
        input: Seq,
        next: Ind,
        score: usize,
    ) -> Self {
        Self {
            score,
            input: input.into(),
            next: next.into(),
        }
    }
}
