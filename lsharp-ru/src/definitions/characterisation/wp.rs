use std::collections::{BTreeMap, BTreeSet};

use itertools::Itertools;
use rustc_hash::FxHashSet;

use crate::definitions::mealy::{InputSymbol, Mealy, State};
use crate::definitions::FiniteStateMachine;

use super::{Characterisation, WMethod};

/// Construct a characterisation set using the Wp-Method for a given FSM.
///
#[derive(Debug)]
#[allow(clippy::module_name_repetitions)]
pub struct WpMethod;

impl WpMethod {
    /// Compute an iterator for the Wp-characterisation set of `state`.
    ///
    /// The iterator contains sequences s.t. there is at least one sequence which separates
    /// `state` from another state in the FSM. The returned iterator has unique elements,
    ///  i.e., no sequence is repeated.
    ///
    /// # Example
    /// ```
    /// # use crate::lsharp_ru::{
    /// #    definitions::{
    /// #        mealy::{Mealy, State},
    /// #        FiniteStateMachine,
    /// #    },
    /// #      util::parsers::machine::read_mealy_from_file,
    /// # };
    /// # use lsharp_ru::definitions::characterisation::WpMethod;
    /// # use itertools::Itertools;
    /// let (fsm,_,_) = read_mealy_from_file("tests/src_models/trial.dot");
    /// let states: Vec<_> = fsm.states().into_iter().collect();
    /// let char_set: Vec<_> = WpMethod::char_set(&fsm).collect();
    /// let separates = |x, y, seq| fsm.trace_from(x, seq).1 != fsm.trace_from(y, seq).1;
    /// for (x, y) in Itertools::tuple_combinations(states.into_iter()) {
    ///        assert!(char_set.iter().any(|seq| separates(x, y, seq)));
    ///  }
    /// ```
    pub fn char_set(fsm: &Mealy) -> impl Iterator<Item = Vec<InputSymbol>> + '_ {
        WMethod::char_set(fsm)
    }
}

impl Characterisation for WpMethod {
    fn characterisation_map(fsm: &Mealy) -> BTreeMap<State, BTreeSet<Vec<InputSymbol>>> {
        let mut ident = BTreeMap::<State, BTreeSet<Vec<InputSymbol>>>::default();
        let pairs = fsm.states().into_iter().tuple_combinations();
        let splits =
            |seq: Vec<InputSymbol>, s, t| fsm.trace_from(s, &seq).1 != fsm.trace_from(t, &seq).1;
        let mut pairs_split = FxHashSet::default();
        for seq in Self::char_set(fsm) {
            for (s, t) in pairs.clone() {
                if pairs_split.contains(&(s, t)) {
                    continue;
                }
                if splits(seq.clone(), s, t) {
                    ident.entry(s).or_default().insert(seq.clone());
                    ident.entry(t).or_default().insert(seq.clone());
                    pairs_split.insert((s, t));
                }
            }
        }
        ident
    }
}

#[cfg(test)]
mod tests {
    use std::path::Path;

    use rstest::rstest;

    use crate::definitions::characterisation::Characterisation;
    use crate::definitions::FiniteStateMachine;
    use crate::{definitions::mealy::Mealy, util::parsers::machine::read_mealy_from_file};

    fn load_basic_fsm(name: &str) -> Mealy {
        let file_name = Path::new(name);
        read_mealy_from_file(file_name.to_str().expect("Safe")).0
    }

    /// Ensure that the initial state is set.
    #[rstest]
    #[case::coffee_machine("tests/src_models/trial.dot")]
    #[case("tests/src_models/w_test.dot")]
    #[case("tests/src_models/hypothesis_23.dot")]
    fn fsm_wp_separates(#[case] file_name: &str) {
        let fsm = load_basic_fsm(file_name);
        #[allow(clippy::needless_collect)]
        let states: Vec<_> = fsm.states().into_iter().collect();
        let char_set: Vec<_> = super::WpMethod::char_set(&fsm).collect();
        let separates = |x, y, seq| fsm.trace_from(x, seq).1 != fsm.trace_from(y, seq).1;
        for (x, y) in itertools::Itertools::tuple_combinations(states.into_iter()) {
            assert!(char_set.iter().any(|seq| separates(x, y, seq)));
        }
    }

    #[rstest]
    #[case::coffee_machine("tests/src_models/trial.dot")]
    #[case("tests/src_models/w_test.dot")]
    #[case("tests/src_models/BitVise.dot")]
    fn reproducible(#[case] file_name: &str) {
        let fsm = load_basic_fsm(file_name);
        let char_set = super::WpMethod::characterisation_map(&fsm);
        for _ in 0..5 {
            assert_eq!(char_set, super::WpMethod::characterisation_map(&fsm));
        }
    }

    #[rstest]
    #[case::coffee_machine("tests/src_models/trial.dot")]
    #[case("tests/src_models/w_test.dot")]
    #[case("tests/src_models/BitVise.dot")]
    fn wp_size_cmp_w(#[case] file_name: &str) {
        let fsm = load_basic_fsm(file_name);
        let w_char_map = super::WMethod::characterisation_map(&fsm);
        let partial_w_char_map = super::WpMethod::characterisation_map(&fsm);
        for s in fsm.states() {
            let w_char_local = w_char_map.get(&s).expect("Safe");
            let partial_w_char_local = partial_w_char_map.get(&s).expect("Safe");
            let w_char_local_len = w_char_local.len();
            let partial_w_char_local_len = partial_w_char_local.len();
            assert!(partial_w_char_local_len <= w_char_local_len);
        }
    }
}
