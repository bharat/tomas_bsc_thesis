use std::collections::{BTreeMap, BTreeSet};

use itertools::Itertools;
use rustc_hash::FxHashSet;

use crate::definitions::{
    mealy::{InputSymbol, Mealy, State},
    FiniteStateMachine,
};

use super::Characterisation;

/// Construct a characterisation set using the W-Method for a given FSM.
///
#[derive(Debug)]
#[allow(clippy::module_name_repetitions)]
pub struct WMethod;

#[derive(Debug, Clone)]
pub(super) struct Partition {
    group: BTreeMap<usize, Vec<State>>,
    next: BTreeMap<State, Vec<(State, usize)>>,
}

impl Partition {
    pub fn new(
        group: BTreeMap<usize, Vec<State>>,
        next: BTreeMap<State, Vec<(State, usize)>>,
    ) -> Self {
        Self { group, next }
    }

    fn splits(&self, s1: State, s2: State) -> bool {
        self.group.values().any(|grp| {
            (grp.contains(&s1) && !grp.contains(&s2)) || (!grp.contains(&s1) && grp.contains(&s2))
        })
    }

    fn dist_symbol(&self, s: State, t: State) -> InputSymbol {
        let s_next = self.next.get(&s).expect("Safe");
        let t_next = self.next.get(&t).expect("Safe");
        for (i, ((_, g1), (_, g2))) in s_next.iter().zip(t_next.iter()).enumerate() {
            if *g1 != *g2 {
                return InputSymbol::from(i);
            }
        }
        unreachable!()
    }

    fn refine(&self) -> Option<Self> {
        let group_max = self.group.keys().max().copied().expect("Safe") + 1; // group numbering begins at 0.
        if group_max == self.next.len() {
            // |S| == number of groups.
            return None;
        }
        // Find a pair of states to split in the blocks.
        let grps: Vec<_> = self
            .group
            .iter() // for each block in a group
            .filter(|(_, blk)| blk.len() > 1) // block must not be a singleton.
            .collect();

        let (grp_num, to_split) = grps
            .iter()
            .find_map(|(i, block)| {
                let diff_grp = block.iter().into_group_map_by(|s| {
                    self.next
                        .get(*s)
                        .expect("Safe")
                        .iter()
                        .map(|x| x.1)
                        .collect_vec()
                });
                let diff_grp: BTreeMap<_, _> = diff_grp.into_iter().collect();
                if diff_grp.len() > 1 {
                    Some((i, diff_grp.into_values().collect_vec()))
                } else {
                    None
                }
            })
            .expect("Safe");
        let grp_num = **grp_num;
        let new_grp: Vec<_> = to_split
            .into_iter()
            .next()
            .expect("Safe")
            .into_iter()
            .copied()
            .collect();
        let mut ret = (*self).clone();
        // Remove the states which are to be in the new group.
        ret.group.entry(grp_num).and_modify(|v| {
            let z: Vec<_> = v
                .clone()
                .into_iter()
                .filter(|x| !new_grp.contains(x))
                .collect();
            v.clear();
            v.extend(z);
        });
        ret.group
            .entry(group_max)
            .or_default()
            .extend(new_grp.clone());
        for dest_grp_vec in ret.next.values_mut() {
            for (dest, g_num) in dest_grp_vec.iter_mut() {
                if new_grp.contains(dest) {
                    *g_num = group_max;
                }
            }
        }
        Some(ret)
    }
}

impl WMethod {
    /// Compute an iterator for the W-characterisation set of `state`.
    ///
    /// The iterator contains sequences s.t. there is at least one sequence which separates
    /// `state` from another state in the FSM. The returned iterator has unique elements,
    ///  i.e., no sequence is repeated.
    ///
    /// # Example
    /// ```
    /// # use crate::lsharp_ru::{
    /// #    definitions::{
    /// #        mealy::{Mealy, State},
    /// #        FiniteStateMachine,
    /// #    },
    /// #      util::parsers::machine::read_mealy_from_file,
    /// # };
    /// # use lsharp_ru::definitions::characterisation::WMethod;
    /// # use itertools::Itertools;
    /// let (fsm,_,_) = read_mealy_from_file("tests/src_models/trial.dot");
    /// let states: Vec<_> = fsm.states().into_iter().collect();
    /// let char_set: Vec<_> = WMethod::char_set(&fsm).collect();
    /// let separates = |x, y, seq| fsm.trace_from(x, seq).1 != fsm.trace_from(y, seq).1;
    /// for (x, y) in Itertools::tuple_combinations(states.into_iter()) {
    ///        assert!(char_set.iter().any(|seq| separates(x, y, seq)));
    ///  }
    /// ```
    pub fn char_set(fsm: &Mealy) -> impl Iterator<Item = Vec<InputSymbol>> + '_ {
        let mut ret: Vec<Vec<_>> = Vec::default();

        let fsm_states: Vec<State> = fsm.states().into_iter().collect();
        let input_alphabet = fsm.input_alphabet();

        // Construct the output partitions.
        let mut p1_part_out: BTreeMap<Vec<_>, Vec<_>> = BTreeMap::default();
        for s in &fsm_states {
            let out_vec: Vec<_> = input_alphabet
                .iter()
                .map(|i| fsm.step_from(*s, *i).1)
                .collect();
            p1_part_out.entry(out_vec).or_default().push(*s);
        }

        // All states with the same outputs go to the same block.
        let mut p1_part: BTreeMap<usize, BTreeMap<_, _>> = BTreeMap::default();
        for (block_num, block) in p1_part_out.into_values().enumerate() {
            for s in block {
                let dest_vec: Vec<_> = input_alphabet
                    .iter()
                    .map(|i| fsm.step_from(s, *i).0)
                    .collect();
                p1_part.entry(block_num).or_default().insert(s, dest_vec);
            }
        }
        let p1_part = {
            let group: BTreeMap<_, _> = fsm_states
                .iter()
                .map(|s| {
                    let num = p1_part
                        .iter()
                        // find the block with state s.
                        .find(|(_num, dest_map)| dest_map.contains_key(s))
                        .map(|(n, _)| n);
                    (*s, *num.expect("Safe"))
                })
                .collect();

            let next: BTreeMap<_, _> = p1_part
                .into_values()
                .flatten()
                .map(|(s, dest_vec)| {
                    let dest_part: Vec<(State, usize)> = dest_vec
                        .into_iter()
                        .map(|d| (d, *group.get(&d).expect("Safe")))
                        .collect();
                    (s, dest_part)
                })
                .collect();

            let group: BTreeMap<usize, Vec<State>> =
                group
                    .into_iter()
                    .fold(BTreeMap::default(), |mut acc, (state, grp)| {
                        acc.entry(grp).or_default().push(state);
                        acc
                    });
            Partition::new(group, next)
        };
        let mut parts = vec![p1_part.clone()];
        let mut curr = p1_part;
        while let Some(next) = curr.refine() {
            parts.push(next.clone());
            curr = next;
        }
        ret.extend(
            fsm_states
                .iter()
                .copied()
                .tuple_combinations()
                .map(|(s, t)| Self::find_sep_seq(fsm, &parts, s, t))
                .unique(),
        );
        ret.sort_unstable_by_key(|b| std::cmp::Reverse(b.len()));
        let mut split_pairs = FxHashSet::default();
        let splits =
            |seq: Vec<InputSymbol>, s, t| fsm.trace_from(s, &seq).1 != fsm.trace_from(t, &seq).1;

        let mut char_set = Vec::new();
        for seq in &ret {
            let seq_splits: FxHashSet<_> = fsm_states
                .iter()
                .copied()
                .tuple_combinations()
                .filter(|(s, t)| splits(seq.clone(), *s, *t))
                .collect();
            if seq_splits.is_subset(&split_pairs) {
                continue;
            }
            char_set.push(seq.clone());
            split_pairs.extend(seq_splits.into_iter());
        }
        char_set.into_iter()
    }

    fn find_sep_seq(fsm: &Mealy, parts: &[Partition], s: State, t: State) -> Vec<InputSymbol> {
        let mut ret = vec![];
        let mut rev_parts: Vec<_> = parts.to_vec();
        rev_parts.reverse();
        let mut s = s;
        let mut t = t;
        for (part_n, part_n_1) in rev_parts.iter().tuple_windows() {
            if !part_n.splits(s, t) || part_n_1.splits(s, t) {
                continue;
            }
            let i = part_n_1.dist_symbol(s, t);
            s = fsm.step_from(s, i).0;
            t = fsm.step_from(t, i).0;
            ret.push(i);
        }
        let i = fsm
            .input_alphabet()
            .into_iter()
            .find(|i| fsm.step_from(s, *i).1 != fsm.step_from(t, *i).1)
            .expect("Safe");
        ret.push(i);
        ret
    }
}

impl Characterisation for WMethod {
    fn characterisation_map(fsm: &Mealy) -> BTreeMap<State, BTreeSet<Vec<InputSymbol>>> {
        let cset: BTreeSet<_> = Self::char_set(fsm).collect();
        fsm.states()
            .into_iter()
            .map(|s| (s, cset.clone()))
            .collect()
    }
}
