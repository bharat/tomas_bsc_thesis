use clap::Parser;
#[derive(Parser, derive_getters::Getters)]
#[command(author, version, about)]
pub struct Cli {
    #[arg(short = 'm', long)]
    model: String,
    #[arg(long)]
    traces: Option<String>,
    /// Use suffixes.
    #[arg(long)]
    use_suffixes: bool,
    /// Full path to the gsuffix executable.
    #[arg(long)]
    gsuffix: String,
    /// Number of extra states.
    #[arg(short = 'k')]
    extra_states: usize,
    #[arg(short = 's')]
    /// Length of the suffix to look for.
    len_suffix: usize,
    #[arg(short = 'v')]
    /// Set verbosity level for logs.
    ///
    /// 0 => disabled, 1 => Info, 2 => Debug, 3 => Trace.
    verbosity: usize,
}
