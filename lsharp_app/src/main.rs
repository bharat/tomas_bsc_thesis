use clap::Parser;
use fnv::FnvBuildHasher;
use itertools::Itertools;
use log::LevelFilter;
use log4rs::{
    append::file::FileAppender,
    config::{Appender, Root},
    encode::pattern::PatternEncoder,
    Config,
};
use lsharp_ru::{
    definitions::{
        mealy::{shortest_separating_sequence, InputSymbol, Mealy, OutputSymbol},
        FiniteStateMachine,
    },
    learner::{
        l_sharp::Lsharp,
        obs_tree::{compressed::CompObsTree, normal::MapObsTree},
    },
    oracles::{equivalence::EquivalenceOracle, membership::Oracle as OQOracle},
    sul::{Simulator, SinkWrapper, SystemUnderLearning},
    util::parsers::logs::read as read_logs,
    util::writers::overall as MealyWriter,
};
use std::{cell::RefCell, collections::HashMap, fs, hash::BuildHasherDefault, rc::Rc, sync::Arc};

use crate::suffix_tree_oracle::SuffixOracle;

mod cli;
mod suffix_tree_oracle;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    type DefaultFxHasher = BuildHasherDefault<rustc_hash::FxHasher>;
    ensure_dir_exists("./hypothesis")?;

    let logfile = FileAppender::builder()
        .encoder(Box::new(PatternEncoder::new(
            "{d(%Y-%m-%d %H:%M:%S)} {l} {M} | {m}{n}",
        )))
        .build(format!("log/{}.log", chrono::Utc::now()))?;
    let args = cli::Cli::parse();

    let logging_level = *args.verbosity();
    let logging_level = match logging_level {
        0 => LevelFilter::Off,
        1 => LevelFilter::Info,
        2 => LevelFilter::Debug,
        _ => LevelFilter::Trace,
    };

    let config = Config::builder()
        .appender(Appender::builder().build("logfile", Box::new(logfile)))
        .build(Root::builder().appender("logfile").build(logging_level))?;

    log4rs::init_config(config)?;

    let file_name = args.model();
    let log_path = args.traces();
    let gsuffix_path = args.gsuffix();
    let use_suffixes = *args.use_suffixes();
    let extra_states = *args.extra_states();
    let suffix_len = *args.len_suffix();
    let seed = 5;
    let (fsm, input_map, output_map) =
        prep_fsm_from_file(file_name.as_str()).expect("Error reading from DOT file");
    let sink_output_asml = *output_map
        .get("error")
        .unwrap_or(&OutputSymbol::new(u16::MAX));
    let fsm_logs = log_path
        .as_ref()
        .and_then(|log_p| read_logs(log_p, &input_map, &output_map).ok());
    let mealy_machine = Arc::new(fsm.clone());
    let trial_wo_sink = Simulator::new(&mealy_machine, &input_map, &output_map);

    let mut trial = SinkWrapper::new(trial_wo_sink, sink_output_asml);
    // let comp_tree = CompObsTree::<DefaultFxHasher>::new(trial.input_map().len());
    let naive_tree = MapObsTree::<DefaultFxHasher>::new(trial.input_map().len());

    let oq_oracle = Rc::new(RefCell::new(OQOracle::new(
        &mut trial,
        lsharp_ru::learner::l_sharp::Rule2::Ads,
        lsharp_ru::learner::l_sharp::Rule3::Ads,
        sink_output_asml,
        seed,
        naive_tree,
    )));
    let logs = fsm_logs.clone().map(|logs| {
        logs.into_iter()
            .map(|(x, y)| (x.to_vec(), y.to_vec()))
            .collect_vec()
    });

    // Check traces
    {
        let rev_input_map: HashMap<_, _> = input_map
            .iter()
            .map(flip)
            .map(|(x, y)| (*x, y.clone()))
            .collect();
        let rev_output_map: HashMap<_, _> = output_map
            .iter()
            .map(flip)
            .map(|(x, y)| (*x, y.clone()))
            .collect();
        let traces = logs.clone().expect("Safe");
        for (input_trace, expected_output) in traces {
            let actual_output = fsm.trace(&input_trace).1.to_vec();
            // assert_eq!(actual_output, expected_output);
            if actual_output != expected_output {
                let input_word: String = input_trace
                    .iter()
                    .map(|x| rev_input_map.get(x).expect("Safe").clone())
                    .intersperse("".to_string())
                    .collect();
                let exp_word: String = expected_output
                    .iter()
                    .map(|x| rev_output_map.get(x).expect("Safe").clone())
                    .intersperse("".to_string())
                    .collect();
                let actual_word: String = actual_output
                    .iter()
                    .map(|x| rev_output_map.get(x).expect("Safe").clone())
                    .intersperse("".to_string())
                    .collect();

                println!("Input word: {}", input_word);
                println!("Exp   word: {}", exp_word);
                println!("Act   word: {}", actual_word);
                panic!("Stopping now!");
            }
        }
    }

    assert_eq!(logs, fsm_logs);

    // The suffix oracle goes here.
    let mut eq_oracle = SuffixOracle::new(
        Rc::clone(&oq_oracle),
        gsuffix_path.to_string(),
        extra_states,
        suffix_len,
        log_path.as_ref().expect("Traces not provided."),
        &input_map,
        use_suffixes,
    );

    // And Learner.
    let mut learner = Lsharp::new(Rc::clone(&oq_oracle), input_map.len(), false);

    let mut idx = 1;
    let mut learn_inputs: usize = 0;
    let mut learn_resets: usize = 0;
    let mut test_inputs: usize = 0;
    let mut test_resets: usize = 0;
    let mut success = false;
    // Set this to whatever you like.

    learner.init_obs_tree(logs);

    loop {
        println!("LOOP {idx}");
        let hypothesis = learner.build_hypothesis();
        println!("Hyp size: {}", hypothesis.states().len());
        let (l_inputs, l_resets) = learner.get_counts();
        learn_inputs += l_inputs;
        learn_resets += l_resets;
        println!("Learning queries/symbols: {l_resets}/{l_inputs}",);

        let writer_config = MealyWriter::WriteConfigBuilder::default()
            .file_name(Some(
                format!("./hypothesis/hypothesis_{idx}.dot").to_string(),
            ))
            .format(MealyWriter::MealyEncoding::Dot)
            .build()
            .expect("Could not write Hypothesis file.");
        {
            let (input_vec, output_vec) = RefCell::borrow(&oq_oracle).pass_maps();
            let rev_input_map = input_vec.into_iter().map(flip).collect();
            let rev_output_map = output_vec.into_iter().map(flip).collect();
            MealyWriter::write_machine::<FnvBuildHasher, _>(
                &writer_config,
                &hypothesis,
                &rev_input_map,
                &rev_output_map,
            )
            .expect("Safe");
        }

        let ideal_ce = shortest_separating_sequence(&fsm, &hypothesis, None, None);
        log::info!("Ideal ce: {:?}", ideal_ce);
        if ideal_ce.is_none() {
            success = true;
            break;
        }
        {
            let (input_vec, _output_vec) = RefCell::borrow(&oq_oracle).pass_maps();
            let rev_input_map: HashMap<_, _, FnvBuildHasher> =
                input_vec.into_iter().map(flip).collect();
            let id_ce = ideal_ce.expect("Safe");
            let id_ce: String = id_ce
                .0
                .into_iter()
                .map(|i| rev_input_map.get(&i).expect("Safe"))
                .cloned()
                .intersperse(" ".to_string())
                .collect();
            log::info!("Ideal ce: {}", id_ce)
        }
        let ce = eq_oracle.find_counterexample(&hypothesis);
        let (t_inputs, t_resets) = eq_oracle.get_counts();
        test_inputs += t_inputs;
        test_resets += t_resets;
        println!("Testing queries/symbols: {t_resets}/{t_inputs}");
        if ce.is_some() {
            {
                let (input_vec, _output_vec) = RefCell::borrow(&oq_oracle).pass_maps();
                let rev_input_map: HashMap<_, _, FnvBuildHasher> =
                    input_vec.into_iter().map(flip).collect();
                let id_ce = ce.clone().expect("Safe");
                let fsm_output = fsm.trace(&id_ce.0).1;
                let hyp_output = hypothesis.trace(&id_ce.0).1;
                assert_ne!(fsm_output, hyp_output);
                let id_ce: String = id_ce
                    .0
                    .into_iter()
                    .map(|i| rev_input_map.get(&i).expect("Safe"))
                    .cloned()
                    .intersperse(" ".to_string())
                    .collect();
                log::info!("Received ce: {}", id_ce)
            }
            println!("CE found, refining observation tree!");
            learner.process_cex(ce, &hypothesis);
        } else {
            println!("No CE found, learning finished.");
            break;
        }
        idx += 1
    }
    println!("Learned : {success}");
    println!("MQ [Resets] : {learn_resets}");
    println!("MQ [Symbols] : {learn_inputs}");
    println!("EQ [Resets] : {test_resets}");
    println!("EQ [Symbols] : {test_inputs}");
    println!("Rounds : {idx}");
    Ok(())
}

fn ensure_dir_exists(dir_name: &str) -> std::io::Result<()> {
    // Do not remove any of these lines
    // Remove the dir (returns an error if it doesn't exist)
    // and ignore the error-case.
    let _ = fs::remove_dir_all(dir_name);
    // Create an empty dir.
    fs::create_dir_all(dir_name)
}

fn flip<A, B>((x, y): (A, B)) -> (B, A) {
    (y, x)
}
type ParseResultFSM = Result<
    (
        Mealy,
        HashMap<String, InputSymbol>,
        HashMap<String, OutputSymbol>,
    ),
    Box<dyn std::error::Error>,
>;
fn prep_fsm_from_file(file_name: &str) -> ParseResultFSM {
    ensure_dir_exists("./hypothesis")?;
    log::info!("Read file from {:?}", file_name);
    Ok(lsharp_ru::util::parsers::machine::read_mealy_from_file(
        file_name,
    ))
}
